if [[ $TERM == xterm ]]; then TERM=xterm-256color; fi

export EDITOR=nvim

export ZSH="/home/reaton/.oh-my-zsh"

ZSH_THEME="brokebrandon"

DISABLE_UPDATE_PROMPT="true"

plugins=(
 git
 tmux
 zsh-autosuggestions
 zsh-syntax-highlighting
)

source $ZSH/oh-my-zsh.sh
path+=('$HOME/.cargo/env')
export PATH

export PATH=/home/reaton/.local/bin:$PATH

alias vim="nvim"
alias zshconfig="$EDITOR ~/.zshrc"
alias gtl="~/./.local/bin/gtl --mode tui"
alias mac="macchina --no-box -X Host Kernel Distribution  WindowManager Shell"
alias ls="exa"
# Debian-based alias
#alias update="sudo apt update -y && sudo apt full-upgrade -y && sudo apt autoclean -y && sudo apt autoremove -y"
# Arch-based alias
alias update="sudo pacman -Syu"

#########################################################
#                                                       #
#                        42                             #
#                                                       #
#########################################################
export MAIL="hmechich@student.42.fr"
export USER="hmechich"
alias g42="clang -Wall -Wextra -Werror -fsanitize=address -g3"
alias norm="norminette -R CheckForbiddenSourceHeader"
