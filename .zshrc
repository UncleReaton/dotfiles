if [[ $TERM == xterm  ]]; then TERM=xterm-256color; fi

export EDITOR=nvim

#########################################################
#                                                       #
#                        PROMPT                         #
#                                                       #
#########################################################

# Load version control info
autoload -Uz vcs_info
precmd() { vcs_info }

# Format the vcs_info_msg_0_ var
zstyle ':vcs_info:git:*' formats '[%b]'

#Set up the prompt (with the git branch name)
setopt PROMPT_SUBST
NEWLINE=$'\n'
PROMPT='%F{cyan}[%f%F{green}%n@%m%f %~%F{cyan}]%f%F{red}${vcs_info_msg_0_}%f${NEWLINE}» '


#########################################################
#                                                       #
#                        VI MODE                        #
#                                                       #
#########################################################
bindkey -v

KEYTIMEOUT=1

# Change cursor shape for different vi modes.
function zle-keymap-select {
        if [[ ${KEYMAP} == vicmd ]] ||
        [[ $1 = 'block' ]]; then
        echo -ne '\e[2 q'

        elif [[ ${KEYMAP} == main ]] ||
                [[ ${KEYMAP} == viins ]] ||
                [[ ${KEYMAP} = '' ]] ||
                [[ $1 = 'line' ]]; then
                echo -ne '\e[6 q'
        fi
}
zle -N zle-keymap-select
zle-line-init() {
        zle -K viins
        echo -ne "\e[6 q"
}
zle -N zle-line-init
echo -ne "\e[2 q"
preexec() { echo -ne "\e[2 q" }

#########################################################
#                                                       #
#                        ALIASES                        #
#                                                       #
#########################################################
source $HOME/.zsh_aliases

#########################################################
#                                                       #
#                        plugins                        #
#                                                       #
#########################################################
source $HOME/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source $HOME/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh