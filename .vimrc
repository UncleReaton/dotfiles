set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

Plugin 'rust-lang/rust.vim'
Plugin 'patstockwell/vim-monokai-tasty'
Plugin 'preservim/nerdtree'
Plugin 'jiangmiao/auto-pairs'
Plugin 'itchyny/lightline.vim'
"Plugin 'vim-syntastic/syntastic'
"Plugin 'alexandregv/norminette-vim'
Plugin 'pbondoer/vim-42header'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

set shell=/bin/zsh
syntax on
syntax enable
set noshowmode
set noswapfile
let g:vim_monokai_tasty_italic = 1
colorscheme vim-monokai-tasty
set background=dark
"set term=xterm-256color
set colorcolumn=80
set number
set autoindent
set smartindent
set cursorline
set scrolloff=10
set ruler
set shiftwidth=4
set tabstop=4

" NERDTREE
let mapleader=','
nnoremap <leader>n :NERDTreeFocus<CR>
nnoremap <C-n> :NERDTree<CR>
nnoremap <C-t> :NERDTreeToggle<CR>
nnoremap <C-f> :NERDTreeFind<CR>

" Lightline
let g:lightline = {
      \ 'colorscheme': 'monokai_tasty',
      \ }
