# Additional infos

## YADM
My dotfiles are managed by [yadm](https://yadm.io/).

## Warning
I'm mostly tweaking around my dotfiles to make them suit me more which makes it a bit of a mess.

## Terminal
I use [Kitty](https://sw.kovidgoyal.net/kitty/) terminal

## Tools that I like
- [fd](https://github.com/sharkdp/fd) = alternative to find
- [exa](https://github.com/ogham/exa) = alternative to ls
- [bat](https://github.com/sharkdp/bat) = alternative to cat
